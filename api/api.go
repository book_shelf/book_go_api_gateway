package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"book_shelf/book_go_api_gateway.git/api/docs"
	"book_shelf/book_go_api_gateway.git/api/handlers"
	"book_shelf/book_go_api_gateway.git/config"
)

// New
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))

	r.GET("/config", h.GetConfig)

	// r.POST("/login", h.Login)

	// User_service >> User
	r.POST("/user", h.CreateUser)
	r.GET("/user/:id", h.GetUserByID)
	r.GET("/user", h.GetUserList)
	r.PUT("/user/:id", h.UpdateUser)
	r.PATCH("/user/:id", h.UpdatePatchUser)
	r.DELETE("/user/:id", h.DeleteUser)

	// User_service >> Region
	r.POST("/region", h.CreateRegion)
	r.GET("/region/:id", h.GetRegionByID)
	r.GET("/region", h.GetRegionList)
	r.PUT("/region/:id", h.UpdateRegion)
	r.DELETE("/region/:id", h.DeleteRegion)

	// User_service >> District
	r.POST("/district", h.CreateDistrict)
	r.GET("/district/:id", h.GetDistrictByID)
	r.GET("/district", h.GetDistrictList)
	r.PUT("/district/:id", h.UpdateDistrict)
	r.DELETE("/district/:id", h.DeleteDistrict)

	// User_service >> Book
	r.POST("/book", h.CreateBook)
	r.GET("/book/:id", h.GetBookByID)
	r.GET("/book", h.GetBookList)
	r.PUT("/book/:id", h.UpdateBook)
	r.PATCH("/book/:id", h.UpdatePatchBook)
	r.DELETE("/book/:id", h.DeleteBook)

	// Save Image
	r.POST("/upload/", h.UploadImage)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}
