package handlers

import (
	"fmt"
	"book_shelf/book_go_api_gateway.git/api/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// UploadImage godoc
// @ID upload_image
// @Router /upload/ [POST]
// @Summary Upload Image
// @Description  Upload image and get the url
// @Tags Image
// @Accept mpfd
// @Produce json
// @Param file formData file true "Image file"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
func (h *Handler) UploadImage(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil{
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	if file.Size >10<<20{
		h.handleResponse(c, http.BadRequest, "file size out of range")
		return
	}

	fileName := fmt.Sprintf("%s_%s", uuid.New().String(), file.Filename)

	err = c.SaveUploadedFile(file, "assets/"+fileName)
	if err != nil{
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	url := "http://localhost:8001/assets/" + fileName

	h.handleResponse(c, http.OK, url)
}