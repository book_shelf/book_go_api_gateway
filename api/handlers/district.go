package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"book_shelf/book_go_api_gateway.git/api/http"
	"book_shelf/book_go_api_gateway.git/genproto/user_service"
	"book_shelf/book_go_api_gateway.git/pkg/util"
)

// CreateDistrict godoc
// @ID create_district
// @Router /district [POST]
// @Summary Create District
// @Description  Create District
// @Tags District
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body user_service.CreateDistrict true "CreateDistrictRequestBody"
// @Success 200 {object} http.Response{data=user_service.District} "GetDistrictBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateDistrict(c *gin.Context) {

	var district user_service.CreateDistrict

	err := c.ShouldBindJSON(&district)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.DistrictService().Create(
		c.Request.Context(),
		&district,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetDistrictByID godoc
// @ID get_district_by_id
// @Router /district/{id} [GET]
// @Summary Get District  By ID
// @Description Get District  By ID
// @Tags District
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.District} "DistrictBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetDistrictByID(c *gin.Context) {

	districtID := c.Param("id")

	if !util.IsValidUUID(districtID) {
		h.handleResponse(c, http.InvalidArgument, "district id is an invalid uuid")
		return
	}

	resp, err := h.services.DistrictService().GetById(
		context.Background(),
		&user_service.DistrictPrimaryKey{
			DistrictId: districtID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetDistrictList godoc
// @ID get_district_list
// @Router /district [GET]
// @Summary Get District s List
// @Description  Get District s List
// @Tags District
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=user_service.GetListDistrictResponse} "GetAllDistrictResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetDistrictList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.DistrictService().GetList(
		context.Background(),
		&user_service.GetListDistrictRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateDistrict godoc
// @ID update_district
// @Router /district/{id} [PUT]
// @Summary Update District
// @Description Update District
// @Tags District
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateDistrict true "UpdateDistrictRequestBody"
// @Success 200 {object} http.Response{data=user_service.District} "District data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateDistrict(c *gin.Context) {

	var district user_service.UpdateDistrict

	district.DistrictId = c.Param("id")

	if !util.IsValidUUID(district.DistrictId) {
		h.handleResponse(c, http.InvalidArgument, "district id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&district)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.DistrictService().Update(
		c.Request.Context(),
		&district,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteDistrict godoc
// @ID delete_district
// @Router /district/{id} [DELETE]
// @Summary Delete District
// @Description Delete District
// @Tags District
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "District data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteDistrict(c *gin.Context) {

	districtId := c.Param("id")

	if !util.IsValidUUID(districtId) {
		h.handleResponse(c, http.InvalidArgument, "district id is an invalid uuid")
		return
	}

	resp, err := h.services.DistrictService().Delete(
		c.Request.Context(),
		&user_service.DistrictPrimaryKey{DistrictId: districtId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
