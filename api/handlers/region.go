package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"book_shelf/book_go_api_gateway.git/api/http"
	"book_shelf/book_go_api_gateway.git/genproto/user_service"
	"book_shelf/book_go_api_gateway.git/pkg/util"
)

// CreateRegion godoc
// @ID create_region
// @Router /region [POST]
// @Summary Create Region
// @Description  Create Region
// @Tags Region
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body user_service.CreateRegion true "CreateRegionRequestBody"
// @Success 200 {object} http.Response{data=user_service.Region} "GetRegionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateRegion(c *gin.Context) {

	var region user_service.CreateRegion

	err := c.ShouldBindJSON(&region)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.RegionService().Create(
		c.Request.Context(),
		&region,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetRegionByID godoc
// @ID get_region_by_id
// @Router /region/{id} [GET]
// @Summary Get Region  By ID
// @Description Get Region  By ID
// @Tags Region
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Region} "RegionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetRegionByID(c *gin.Context) {

	regionID := c.Param("id")

	if !util.IsValidUUID(regionID) {
		h.handleResponse(c, http.InvalidArgument, "region id is an invalid uuid")
		return
	}

	resp, err := h.services.RegionService().GetById(
		context.Background(),
		&user_service.RegionPrimaryKey{
			RegionId: regionID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetRegionList godoc
// @ID get_region_list
// @Router /region [GET]
// @Summary Get Region s List
// @Description  Get Region s List
// @Tags Region
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=user_service.GetListRegionResponse} "GetAllRegionResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetRegionList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.RegionService().GetList(
		context.Background(),
		&user_service.GetListRegionRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateRegion godoc
// @ID update_region
// @Router /region/{id} [PUT]
// @Summary Update Region
// @Description Update Region
// @Tags Region
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateRegion true "UpdateRegionRequestBody"
// @Success 200 {object} http.Response{data=user_service.Region} "Region data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateRegion(c *gin.Context) {

	var region user_service.UpdateRegion

	region.RegionId = c.Param("id")

	if !util.IsValidUUID(region.RegionId) {
		h.handleResponse(c, http.InvalidArgument, "region id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&region)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.RegionService().Update(
		c.Request.Context(),
		&region,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteRegion godoc
// @ID delete_region
// @Router /region/{id} [DELETE]
// @Summary Delete Region
// @Description Delete Region
// @Tags Region
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Region data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteRegion(c *gin.Context) {

	regionId := c.Param("id")

	if !util.IsValidUUID(regionId) {
		h.handleResponse(c, http.InvalidArgument, "region id is an invalid uuid")
		return
	}

	resp, err := h.services.RegionService().Delete(
		c.Request.Context(),
		&user_service.RegionPrimaryKey{RegionId: regionId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
