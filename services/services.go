package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"book_shelf/book_go_api_gateway.git/config"
	"book_shelf/book_go_api_gateway.git/genproto/book_service"
	"book_shelf/book_go_api_gateway.git/genproto/user_service"
)

type ServiceManagerI interface {
	UserService() user_service.UserServiceClient
	RegionService() user_service.RegionServiceClient
	DistrictService() user_service.DistrictServiceClient
	BookService() book_service.BookServiceClient
}

type grpcClients struct {
	userService     user_service.UserServiceClient
	regionService   user_service.RegionServiceClient
	districtSerivce user_service.DistrictServiceClient
	bookService     book_service.BookServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Book Service...
	connBookService, err := grpc.Dial(
		cfg.BookServiceHost+cfg.BookGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService:     user_service.NewUserServiceClient(connUserService),
		regionService:   user_service.NewRegionServiceClient(connUserService),
		districtSerivce: user_service.NewDistrictServiceClient(connUserService),
		bookService:     book_service.NewBookServiceClient(connBookService),
	}, nil
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

func (g *grpcClients) RegionService() user_service.RegionServiceClient {
	return g.regionService
}

func (g *grpcClients) DistrictService() user_service.DistrictServiceClient {
	return g.districtSerivce
}

func (g *grpcClients) BookService() book_service.BookServiceClient {
	return g.bookService
}
